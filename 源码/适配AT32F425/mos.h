#ifndef __MOS_H
#define __MOS_H

#include <stdint.h>

#ifdef __cplusplus
 extern "C" {
#endif 

//------------------------------------------------------------------------------
// 外部函数，在mos.s中实现
//------------------------------------------------------------------------------
void			mos_start								(void);																					// Mini RTOS系统启动函数
uint8_t		mos_create_task	(void *task_address, uint32_t task_stackaddr, uint32_t task_slvs, uint8_t task_priority);	// 创建任务成功返回任务号，失败返回-1
void			mos_task_kill						(uint8_t task_id);															  // 指定结束某任务
void			mos_task_exit						(void);																					// 结束当前任务
int8_t		mos_get_task_id					(void *task_address);														// 获取任务号
void			mos_sleep								(uint16_t miliseconds);													// 任务休眠指定毫秒数，最大65535毫秒
void			mos_sleep_task					(uint8_t task_id, uint16_t msecs);							// 指定某任务休眠指定毫秒数，最大65535毫秒
uint32_t	*mos_malloc							(uint16_t size);																// 动态申请堆内存，最大0xFFFC字节，释放后的堆内存由空闲函数予以回收后才能再使用
uint8_t		mos_free								(uint32_t *memory);															// 动态释放堆内存，mos_malloc和mos_free函数最好成对使用，避免内存泄露
void			mos_memset				 (uint32_t *buffer, uint32_t value, uint16_t length); // 初始化内存，注意长度单位是字(4个字节)
uint8_t		mos_mutex_try_lock			(uint8_t mutex_id);	    												// 尝试锁定0~7号互斥锁，可以被锁定返回0并加锁，已被锁定返回1，非阻塞模式
uint8_t		mos_mutex_lock					(uint8_t mutex_id);															// 加锁0~7号互斥锁，7号锁由mos_malloc函数专用，阻塞模式
uint8_t		mos_mutex_unlock				(uint8_t mutex_id);															// 解除0~7号互斥锁，解锁成功返回0，解锁失败返回-1，mutex_lock和mutex_unlock函数最好成对使用，避免死锁
uint32_t	mos_ipc_mail_send				(uint8_t task_id, uint32_t val);								// 向指定任务号线程的邮箱发送邮件(线程间通信)
uint32_t	mos_ipc_mail_read				(void);                     										// 读取本任务线程邮箱内的邮件(线程间通信)
void			mos_ipc_mail_set				(uint32_t val); 																// 设置本任务线程邮箱内的邮件
uint32_t	mos_set_system_timer		(uint32_t value);																// 设置系统计数器新值并返回系统计数器的旧值
uint32_t	mos_get_system_timer		(void);																					// 获取系统计数器的当前值
uint8_t		mos_get_number_of_tasks	(void);																					// 获取当前工作的任务数
uint8_t		mos_get_current_task_id	(void);																					// 获取当前任务号
uint16_t	mos_get_idle_count			(void);																					// 获取空闲任务在1024毫秒周期内的调度次数，以此计算CPU占用率
uint32_t	mos_get_task_stack_sp		(void);																					// 获取当前任务的SP栈指针
void			mos_trigger_pendsv			(uint8_t flag);																	// 手动触发PendSV异常，用来立即调度任务，可设置旗标为非0xFF的任意值，一般可设为0xF8

#ifdef __cplusplus
}
#endif

#endif /* __MOS_H */
