;------------------------------------------------------------
; Mini RTOS 汇编文件 V0.8.0
;------------------------------------------------------------

				PRESERVE8
                THUMB

				AREA    |.text|, CODE, READONLY

				INCLUDE		mos.inc

;------------------------------------------------------------------------------
; 函数名: pendsv
; 功能: PendSV异常中断，处理任务上下文和任务调度
; 输入参数: 无
; 输出参数: 无
; 注意1: 内部函数
;------------------------------------------------------------------------------
pendsv			PROC
	EXPORT	pendsv
	IMPORT	PendSV_Handler
	push {lr}						; 在异常或中断中lr寄存器的值特殊，需保存后再调用其他函数，后面恢复
	call PendSV_Handler				; 钩子函数中不能定义局部变量，申请动态内存等操作，尽量短小
	pop {r0}
	mov lr,r0						; 恢复lr寄存器的值

	ldr r0, =KERNEL_MEMORY_START	; r0 = 内核堆空间首地址
	ldrb r1,[r0,#OFS_NUM_TASKS]		; 读取内存开始地址偏移OFS_NUM_TASKS保存的当前任务数量
    orrs r1,r1                      ; 判断(根据Z标志)任务数量是否等于0
	beq schedule_idle_task			; 任务数量等于0直接调度空闲任务

    mrs r1,PSP						; r1 = 任务的(PSP线程模式)栈地址，准备保存当前任务(包含普通任务和空闲任务)的上文(r4~r11寄存器及其他由中断时自动保存在栈帧中的)
    subs r1,#8*4					; 为寄存器r4-r11留出栈空间
    stmia r1!,{r4-r7}               ; 以r1为任务栈指针保存r4-r7进任务栈空间，每次入栈后r1自增1
    mov r2,r8                       ; r2, r3在进入异常时由系统自动保存在栈帧中，r4和r5前面也保存过
    mov r3,r9                       ; 所以可利用r2~r5当临时寄存器来保存r8~r11进任务栈空间
    mov r4,r10
    mov r5,r11
    stmia r1!,{r2-r5}               ; 寄存器r4~r11在任务栈空间是按顺序排列的，例如：r4~r11 = 0x20000ee0~0x20000eff
    subs r1,#8*4					; 调整任务栈指针为保存r4-r11寄存器后的新值
    msr PSP,r1                      ; 将调整后的值写回栈指针(PSP线程模式)，自此当前任务的全部寄存器值已保存在任务栈空间中

    ldrb r6,[r0,#OFS_CURRENT_TASK]	; 读取当前任务(1字节，最高位即7位是空闲任务标示)标示，数字标示为：普通任务最大0~7、空闲任务-128(0x80)
    cmp r6,#0x80					; 空闲任务标示是0x80
    bne continue_normal_task        ; 如果不等于0x80，说明不是空闲任务则跳转continue_normal_task继续执行普通任务的处理

	strh r1,[r0,#OFS_IDLE_STACK_POS]; 在内存开始地址偏移OFS_IDLE_STACK_POS处写入空闲任务栈指针，这里利用strh指令截断高位，只保存指针的低16位(2个字节)
    b sleeping_tasks                ; 跳转sleeping_tasks继续执行，如是空闲任务则不执行continue_normal_task这一段普通任务的处理代码

continue_normal_task				; 普通任务的处理
    lsls r1,r6,#TASK_ENTRY_SHIFT_L	; r1得到r6(当前任务号)乘以任务数组(2的TASK_ENTRY_SHIFT_L次方个字节)的大小
    adds r1,#OFS_TASK_ARRAY			; r1 = 当前任务结构数组在整个任务结构数组(从0x20000020开始)中的偏移量
    adds r1,r0						; r1 = 加上内存开始地址，得到当前任务结构数组在内存中的的指针地址
    ldrb r2,[r1,#TASK_ENTRY_STATE]	; 获取当前任务的状态r2 = (r1指针地址+TASK_ENTRY_STATE偏移量)
    cmp r2,#STATE_UNSCHEDULED		; 判断任务状态(r2 - STATE_UNSCHEDULED)是否是不活跃(0x00)或者没有调度(0x01)的状态
    bls sleeping_tasks              ; 如果r2 <= STATE_UNSCHEDULED则跳转sleeping_tasks继续执行，不予保存当前任务栈指针
									; 保存当前任务栈指针动作只会发生在运行态(0x02)和休眠态(0x03)及其以上的各状态(0x04~...)
    mrs r2,PSP						; r2 = 当前任务栈指针(PSP线程模式)
    str r2,[r1,#TASK_ENTRY_STACK]	; 保存当前任务栈指针到当前任务结构数组中

sleeping_tasks						; 空闲任务的处理
    movs r1,#OFS_TASK_ARRAY			; 
    adds r1,r0                      ; r1 = 整个任务结构数组首地址(内核堆空间首地址+内核数组大小)
    movs r2,#MAX_TASKS-1			; r2 = 定义的最大任务的任务号

process_sleeping_tasks				; 根据r2的值递减循环处理所有任务
    lsls r3,r2,#TASK_ENTRY_SHIFT_L	; 获取任务结构数组中的任务偏移量
    adds r3,r1                      ; r3 = 当前任务结构数组在整个任务结构数组中的地址
    ldrb r4,[r3,#TASK_ENTRY_STATE]	; r4 = 任务的状态
    
;	cmp r4,#STATE_INACTIVE			; 判断任务状态是否是不活跃
;	beq advance_counter				; 当前任务是不活跃的，直接跳转到处理下一个任务
;	cmp r4,#STATE_RUNNING			; 判断任务状态是否是运行态
;	beq advance_counter				; 当前任务是运行态，直接跳转到处理下一个任务
	cmp r4,#STATE_RUNNING			; 判断任务状态是否是在运行态或及其以下的状态中(0x00~0x02)
	bls advance_counter				; 如果是，都直接跳转到处理下一个任务
									; 如果不是，则为0x03~0x05状态的处理
    cmp r4,#STATE_WAIT_FOR_MUTEX	; 判断任务状态是否在等待互斥锁(r4 - STATE_WAIT_FOR_MUTEX == 0)
    bne adjust_sleeping_value       ; 如果不是，那么当前任务正在休眠态或由其他任务休眠，跳转adjust_sleeping_value继续处理
									; 如果是，进行任务状态为等待互斥锁的相应处理
check_to_see_if_mutex_is_free
    ldrb r5,[r3,#TASK_ENTRY_MUTEX]	; r5 = 任务数组中的互斥锁号
    ldr r4,[r0,#OFS_MUTEX_STORAGE]	; r4 = 内核数组定义中的互斥锁记录(二进制位表示互斥锁号)
	lsls r5,#2						; r5 = 互斥锁号左移2位(即互斥锁号 = 互斥锁号 * 4)，因为互斥锁记录中每4位记录一把锁
    movs r7,#1
    lsls r7,r5						; r7 = 根据互斥锁号计算出的二进制互斥锁位
	lsls r7,#3						; r7 = 再左移三位，跳过互斥锁记录中锁持有人(任务号)信息位
    tst r4,r7                       ; 检查互斥锁是否可用
	bne advance_counter				; 结果不相等0说明互斥锁不可用，那么不保存互斥锁记录，跳转到处理下一个任务
	; 结果等于0说明互斥锁可用了，继续下面的操作
	; 好不容易等待到互斥锁可用了，赶紧的把加锁结果保存好，让别人没得用
	movs r7,#0xF					; 准备清除数据，以便清除输入对应锁号的二进制位内容
	lsls r7,r5
	mvns r7,r7
	ands r4,r7						; 清除锁号的二进制位内容(即锁的4位内容为0000b)

	mov r7,r2						; 获取当前正在处理的任务号
	adds r7,#8						; 在任务号的高一位二进制位上置1
	lsls r7,r5						; 准备输入对应锁号(任务号+加锁)数据
    orrs r4,r7						; 得到新的输入对应锁号数据(即锁的4位内容为1000b+任务号)

	str r4,[r0,#OFS_MUTEX_STORAGE]	; 写回内核数组中的互斥锁记录
    b set_state_to_running          ; 转到set_state_to_running完成任务继续执行的相关设置

adjust_sleeping_value
	mov r4,r12
	cmp r4,#0xFF					; 根据r12值不同判断，只有从SysTICK过来的情况才进行当前任务的休眠计数器减一处理
	bne advance_counter				; r12的取值：0xFF = systick       函数手动触发
									;            0xF0 = mos_task_exit 函数手动触发
									;            0xF3 = mos_sleep     函数手动触发
									;            0xF8或其他值 = mos_trigger_pendsv 函数手动触发
    ldrh r4,[r3,#TASK_ENTRY_TARGET]	; 读取当前任务的休眠计数器
    subs r4,#1						; 休眠计数器减一
    strh r4,[r3,#TASK_ENTRY_TARGET]	; 写回当前任务的休眠计数器
    bne advance_counter             ; 如果休眠计数器不为零，跳转advance_counter执行检查下一个任务

    ldrb r5,[r3,#TASK_ENTRY_STATE]	; 休眠态(0x03)或由其他任务休眠(0x04)结束了，可以在任务断点继续执行下去
    cmp r5,#STATE_SENT_TO_SLEEP		; 检查是否由其他任务休眠(0x04)的，如果是就不修正栈的偏移量
    beq do_not_advance_pc           ; 如果是，跳转do_not_advance_pc执行，不执行set_state_to_running这一段修正当前任务栈指针的代码

set_state_to_running				; 修正当前任务栈指针的偏移量
    ldr r5,[r3,#TASK_ENTRY_STACK]	; 从当前任务结构数组中获取当前任务栈指针
    ldr r4,[r5,#FRAME_PC+0x20]		; 通过当前任务栈指针获取栈帧中pc寄存器的值(0x20是包含前面r4-r11寄存器压栈后的开销)
    adds r4,#2						; pc = pc + 2，跳过sleep等函数中的“b .”指令，用来恢复当前任务断点的继续执行
    str r4,[r5,#FRAME_PC+0x20]		; 将修正后的pc寄存器值写回当前任务栈空间中，从而继续执行当前任务
    dsb								; 在执行下一条指令之前确保所有内存访问都完成
    
do_not_advance_pc
    movs r4,#STATE_RUNNING			; 设置当前任务为运行态
    strb r4,[r3,#TASK_ENTRY_STATE]

advance_counter
    subs r2,#1						; r2减一处理下一个任务
    bpl process_sleeping_tasks      ; 如果r2大于等于0，则跳转process_sleeping_tasks继续处理完所有任务

; 前期全部任务预处理完成，接下来开始调度具体任务，其中只有在运行态(0x02)或者没有调度(0x01)的任务需要恢复r4~r11寄存器
; 其他任务状态不需要恢复r4~r11寄存器，因为还没有经过中断时发生自动保存寄存器进栈帧
find_next_task_init
    movs r5,#MAX_TASKS				; 再次检查所有任务，寄存器值含义如下
    movs r3,#0						; r3 = 0，设置标志“不是第一次调度”
                                    ; r0 = 内核堆空间首地址
                                    ; r5 = 最大任务数 = 循环查找可调度任务的最大次数
									; r6 = 当前任务号

;	; 最大任务号递减调度方式
;	; 从最大任务号开始向下调度，可以增加实时性，但不能避免互斥锁死锁(或者增加动态优先级功能来解决)
;    movs r6,#MAX_TASKS				; r6 = 最大任务号
;find_next_task
;	; 任务号递减调度方式
;	; 从最大任务号开始检查任务状态，任务号越大优先级越高
;    subs r6,#1						; r6 = r6 - 1(第一次是7，递减任务号)
;    lsls r2,r6,#TASK_ENTRY_SHIFT_L	; 获取任务结构数组中的下一个任务偏移量
;    adds r2,r0  
;    adds r2,#OFS_TASK_ARRAY		; r2 = 任务结构数组中下一个任务的地址

	; 当前任务号递增调度方式
	; 从当前任务号向上开始检查任务状态，所有任务都有机会获得调度，虽然牺牲了一些实时性，但可以解决互斥锁死锁的问题，而且不用复杂的优先级判断，减少代码
find_next_task
    adds r6,#1						; r6 = r6 + 1 = 下一个任务号(递增任务号)
    movs r2,#MAX_TASKS-1			; r2 = 7 = 111b(8个任务的情况下)，重点注意：这里利用&来快速求余，所以r2的值只能是3或者7
    ands r6,r2                      ; r6 = r6 & (MAX_TASKS-1)(获得下一个任务号)
    lsls r2,r6,#TASK_ENTRY_SHIFT_L	; 获取任务结构数组中的下一个任务偏移量
    adds r2,r0  
    adds r2,#OFS_TASK_ARRAY			; r2 = 任务结构数组中下一个任务的地址

    ldrb r1,[r2,#TASK_ENTRY_STATE]	; 获取下一个任务状态
    cmp r1, #STATE_RUNNING			; 判断是否运行态(0x02)
    beq task_found					; 是就进行调度下一个任务
    cmp r1, #STATE_UNSCHEDULED		; 判断是否没有调度(0x01)
    beq it_is_an_unscheduled_task   ; 是就进行调度下一个任务，但不恢复栈指针，因为还没有被调度程序中断过
    subs r5,#1						; 否则r5减一，循环检查所有任务，包括当前的任务，因为它可能是唯一的可调度任务
    bne find_next_task              ; r5不等于0则跳转find_next_task继续执行检查所有任务

schedule_idle_task					; 所有任务不能调度则进入空闲任务的调度
									; r0 = 内核堆空间首地址
	ldrh r1,[r0,#OFS_IDLE_NR_SCHED]	; 读取空闲任务的调度计数器
    adds r1,#1						; 空闲任务的调度计数器加1
    strh r1,[r0,#OFS_IDLE_NR_SCHED]	; 写回空闲任务的调度计数器

    movs r1,#0x80					; 0x80(-128)为空闲任务标示
    strb r1,[r0,#OFS_CURRENT_TASK]	; 保存在当前任务号中

    ldrh r0,[r0,#OFS_IDLE_STACK_POS]; 获取空闲任务栈指针，因为空闲任务栈指针只保存了低16位，使用时还需扩展到32位
	ldr r1,=MCU_MEMORY_START		; r1 = 0x20000000
	adds r0,r1						; 加上0x20000000扩展后才是真实的空闲任务栈指针
    ldr r1,=idle_task+1             ; r1 = 空闲任务程序首地址，总是从空闲任务程序开始处重新执行空闲任务，空闲任务程序的结束部分只能是死循环
    str r1,[r0,#FRAME_PC+0x20]		; 将r1(pc值)写入栈空间(0x20是包含前面r4-r11寄存器压栈后的开销)
    b restore_stack                 ; 跳转restore_stack处开始恢复下文，调度空闲任务

it_is_an_unscheduled_task			; 调度下一个任务，不恢复栈指针
    movs r3,#1						; 改变标志为“第一次调度”
    movs r1,#STATE_RUNNING			; r1 = 运行态(0x01)
									; r2 = 任务结构数组中下一个任务的地址
									; r6 = 下一个任务号
    strb r1,[r2,#TASK_ENTRY_STATE]	; 写回任务状态

task_found							; 已经找到可以调度的下一个任务
    movs r1,r6                      ; r1 = 下一个任务号
    strb r1,[r0,#OFS_CURRENT_TASK]	; 写回当前任务号

    ldr r1,[r2,#TASK_ENTRY_STACK]	; r1 = 下一个任务的任务栈指针
    msr PSP, r1						; 将下一个任务的任务栈指针写入PSP栈指针

    orrs r3,r3                      ; 如果是第一次调度，r3 = 1
    bne pendsv_exit					; 如果r3 == 1跳转pendsv_exit不恢复r4~r11寄存器

    mrs r0,PSP                      ; r0 = PSP栈指针

restore_stack						; 从PSP栈指针(根据r0值的不同，即下一个任务的任务栈指针或者是空闲任务的栈指针)恢复任务的下文(r4~r11寄存器及其他由中断时自动保存在栈帧中的)
	ldmia r0!,{r4-r7}               ; 顺序恢复r4~r7，r0 = r0 + 0x10(ldmia r0!自动实现的开销)
    ldr r1,[r0,#0x00]               ; 使用r1~r3临时保存恢复值以便恢复至r8~r11
    ldr r2,[r0,#0x04]
    ldr r3,[r0,#0x08]
    mov r8,r1
    mov r9,r2
    mov r10,r3
    ldr r1,[r0,#0x0c]
    mov r11,r1						; r4~r11寄存器恢复完毕

	adds r0,#0x10					; r0 = r0 + 0x10(恢复r8~r11的开销)
    msr PSP,r0                      ; 写入PSP指针

pendsv_exit
	isb								; 在执行新指令之前，确保所有前面的指令都已完成
    ret                             ; 调度新任务（任务数量为0时直接调度空闲任务）
									; 中断结束时自动恢复由中断开始时自动保存在栈帧中的8个寄存器
				ENDP

;------------------------------------------------------------------------------
; 函数名: systick
; 功能: systick异常中断，每1ms一次调用PendSV异常中断处理上下文
; 输入参数: 无
; 输出参数: 无
; 注意1: 内部函数
;------------------------------------------------------------------------------
systick			PROC
	EXPORT	systick
	IMPORT	SysTick_Handler
	push {lr}						; 在异常或中断中的lr值与普通函数调用不同，调用其他函数前需保存lr值后面恢复
	call SysTick_Handler			; 钩子函数中不能定义局部变量，申请动态内存等操作，尽量短小
	pop {r0}
	mov lr,r0						; 恢复lr值

	ldr r0, =KERNEL_MEMORY_START	; r0 = 内核堆空间首地址
    
    ldr r1,[r0,#OFS_SYS_TIMER]		; 读取内存开始地址偏移OFS_SYS_TIMER的SYS_TIMER值
    adds r1,#1
    str r1,[r0,#OFS_SYS_TIMER]		; SYS_TIMER加一后保存

	ldr r2,=0x3FF					; 对SYS_TIMER快速求余
	ands r2,r1,r2					; 结果等于0~1023
	ldr r1,=1023					; 约一秒(1023毫秒)周期将空闲任务的调度计数器置0
	cmp r2,r1						
	bcc idle_task_count_not_clear	; 小于1023跳转
	ldrh r1,[r0,#OFS_IDLE_NR_SCHED]	; 读取空闲任务的调度计数器
    strh r1,[r0,#OFS_IDLE_MAXIMUM_COUNT] ;保存本次周期内最大的空闲任务调度计数器数值，可用于计算cpu占用率
	movs r1,#0
    strh r1,[r0,#OFS_IDLE_NR_SCHED]	; 清零空闲任务的调度计数器数值
idle_task_count_not_clear

	movs r1,#0xFF
	mov r12,r1						; 通过正常SysTICK调用的调度程序，r12 = 0xFF作为判断依据
	trigger_pendsv					; 手动触发PendSV异常，用来立即调度任务
	ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: idle_task
; 功能: 空闲任务将在没有任务可调度时运行，也在所有任务都休眠时尝试回收两个连续的MOS堆空间空闲内存块
;       空闲任务程序最后将一直阻塞，循环到下一个调度来临
; 输入参数: 无
; 输出参数: 无
; 注意1: 内部函数
;------------------------------------------------------------------------------
idle_task		PROC
    ldr r5,=FREE_MEMORY_START   ; r5 = MOS堆空间首地址
    ldr r4,=FREE_MEMORY_END     ; r4 = MOS堆空间末地址
    movs r0,#(MAX_MUTEX-1)		; 尝试加锁7号互斥锁，7号互斥锁是mos_malloc函数专用的，也就是申请动态内存分配期间不能被再次锁定
								; 空闲任务同时还负责堆空间的回收处理，所以期间需要加锁7号互斥锁，暂时停止mos_malloc函数的运行
    call mos_mutex_try_lock
    orrs r0,r0                  ; 
    bne enable_ints             ; 如果锁定不成功，跳转enable_ints进入无限循环，不再进行MOS堆空间的回收工作

    cpsid i                     ; 关中断
idle_loop
    ldr r1,[r5]                 ; r1 = MOS堆空间中本块的控制字(第一次运行时是MOS堆空间首地址中控制字)
    orrs r1,r1                  ; 判断r1 = 本块控制字
    beq idle_release_locks      ; 如果结果是0说明本块未被使用过，跳转idle_release_locks直接结束处理
    bmi check_next_block        ; 如果最高位(31位)是1(说明本块使用中)跳转check_next_block处理下一个块
								; 最高位(31位)不是1，开始处理本块
								; 按设定在本块未使用且不是最后一个块时控制字的值就是本块的大小(按字节表示)
    adds r2,r1,#4               ; r2 = r1 + 4 = 加上控制字大小(4个字节)再加上本块的大小
    adds r2,r5                  ; r2 = r2 + r5，得到下一个块控制字地址
    cmp r2,r4                   ; 比较下一个块控制字地址是否小于MOS堆空间末地址，小于时才处理下一个块
    bhs idle_release_locks      ; 如果>=的话跳转idle_release_locks直接结束处理

    ldr r3,[r2]                 ; r3 = 下一个块控制字
    orrs r3,r3                  ; 判断r3 = 下一个块控制字
    bmi check_next_block        ; 如果最高位(31位)是1(说明下一个块使用中)跳转check_next_block处理下一个块
    bne combine_zones           ; 如果结果不是0说明两个块都空闲，跳转combine_zones进行回收处理
                                ; 如果结果是0，说明下一个块还未使用，但本块需要进行回收，那么先修正r3值
								; 因为按设定最后一个块控制字中的值等于MOS堆空间剩余大小
    subs r3,r4,r2               ; r3 = MOS堆空间末地址 - 下一个块控制字地址 = MOS堆空间剩余大小
    subs r3,#4					; r3 = 再减去控制字大小(4个字节) = 本块的大小

combine_zones
    adds r1,r3					; r1 = 本块的控制字
    adds r1,#4					; r1 = 加上控制字大小(4个字节)
    str r1,[r5]                 ; 回收处理完毕将结果写入到本块的控制字中

check_next_block
    uxth r1,r1                  ; r1 = 高16位清零
    adds r1,#4                  ; r1 = 加上控制字大小(4个字节)
    adds r5,r1                  ; r5 = r5(MOS堆空间基地址逐次增加) + r1

    cmp r5,r4                   ; 比较r5是否小于MOS堆空间末地址
    blo idle_loop               ; 小于跳转idle_loop处理下一个块

idle_release_locks
    movs r0,#(MAX_MUTEX-1)		; 解锁7号互斥锁
    call mos_mutex_unlock

enable_ints
    cpsie i                     ; 开中断
	
    b .                         ; 无限循环，阻塞模式，这里一定要一直阻塞等待下一个PendSV异常调度周期的到来
;	wfi							; 进入睡眠模式可能是比无限循环更好的选择，但使用前注意先设置好睡眠后的唤醒等前置操作
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_malloc
; 功能: 在MOS堆空间中动态分配内存块
; 输入参数: r0 = 需要分配的内存字节数(最大输入数值0x0001~0xFFFC)
; 输出参数: r0 = 0出错，返回0作为空指针
;           r0 = 申请成功，返回新分配的内存地址
; 注意1: 输入的需分配字节数和输出的内存地址都将自动按字(4字节)对齐
; 警告：没有对输入参数进行过多验证，需要尽量保证参数在正确范围内
;------------------------------------------------------------------------------
mos_malloc		PROC
	EXPORT mos_malloc
	push {r4-r5,lr}                 ; 后面有子程序调用，需要先保存lr
    movs r4,r0                      ; 临时保存输入参数，子程序可能会破坏r0，所以先保存
    movs r0,#(MAX_MUTEX-1)          ; 加锁7号互斥锁
    call mos_mutex_lock
    movs r0,r4                      ; 恢复输入参数r0
	
    movs r1,#3						; 确保内存地址4字节对齐（只有4字节对齐了才可以确保单字节，半字，字访问方式均不出错）
    adds r0,#3						; 如果内存地址不对齐，会导致触发硬件异常
    bics r0,r1                      ; r0 = AND(r0, NOT(r1))
    ldr r1,=FREE_MEMORY_START		; r1 = MOS堆空间首地址
    ldr r3,=FREE_MEMORY_END			; r3 = MOS堆空间末地址

check_block
    ldr r2,[r1]                     ; 从内存堆区起始处向上开始查找可供分配的内存块
    orrs r2,r2                      ; 判断控制字r2 = OR(r2)的值
    bmi next_block                  ; 最高位(31位)是1的话这个块在使用，查找下一个可用块
    bne found_a_block               ; 控制字r2 != 0表示这个可用块是分配过的，可用块大小为r2

it_was_never_allocated
    ldr r2,=(FREE_MEMORY_END - FREE_MEMORY_START - 4)	; 如果r2 == 0，表示从未被分配，设置可用块大小为内存堆最大空间减去控制字

found_a_block
    cmp r0,r2                       ; r0 - r2，判断r2可用块大小符合申请要求吗
    bhi next_block                  ; 如果r0 > r2，查找下一个可用块
    subs r5,r2,r0                   ; r5 = 计算可用块剩余的大小
    movs r4,#4                      ; 
    cmp r5,r4                       ; r5还需要大于控制字的大小
    bls next_block                  ; 如果r5 <= 4，查找下一个可用块

    subs r5,r4                      ; r5 = 实际剩余的可用块大小
	
    movs r4,#0x80
    rev r4,r4                       ; r4 = 0x80 变为 0x80000000 (LE <-> BE)
;	ldr r4,=0x80000000				; 小技巧，虽然与上面两句等效，但编译后会多占用4个字节的rom

    orrs r0,r4                      ; 设置r0最高位(31位)为1，表示本块被使用
    str r0,[r1]                     ; 写入控制字
    adds r1,#4						; 加上控制字的大小，这是r1将要返回的内存地址值

    uxth r0,r0                      ; r0 = 高16位清零，以便改写下一个控制字
    adds r0,r1                      ; r0 = 下一个控制字的地址
    str r5,[r0]                     ; 在下一个控制字中写入实际剩余的可用块大小

    b exit_malloc					; 跳转结束处理

next_block
    uxth r2,r2                      ; 设置r2最高位(31位)为0
    adds r1,#4                      ; 加上控制字的大小
    adds r1,r2                      ; r1 = 下一个可用块地址
    cmp r1,r3                       ; 判断是否到内存堆结束地址
    blo check_block					; r1 < r3没到内存堆结束地址则跳转到check_block继续处理内存块分配

exit_with_null_pointer
    movs r1,#0						; 如果已经到内存堆结束地址则分配失败，返回0作为空指针

exit_malloc
    movs r4,r1                      ; 临时保存输出参数
    movs r0,#(MAX_MUTEX-1)			; 解锁7号互斥锁
    call mos_mutex_unlock

    movs r0,r4                      ; 恢复输出参数            
    pop {r4-r5,pc}
    ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_free
; 功能: 释放在MOS堆空间中已分配的内存块
; 输入参数: r0 = 已分配内存的地址
; 输出参数: r0 = 0出错
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_free		PROC
	EXPORT mos_free
    subs r0,#4						; r0 = r0 - 4 指向控制字
    ldr r1,=FREE_MEMORY_START
    cmp r0,r1                       ; 判断地址是否小于内存的起始地址
    blo error_freeing               ; 如果r0 < r1跳转到错误处理

    ldr r1,[r0]
    uxth r1,r1                      ; 使用无符号扩展半字，高位补零
    str r1,[r0]                     ; 置高二字节(16~31位)为0，设置其成为一个未使用的内存块
    ret

error_freeing
    movs r0,#0
    ret								; 出错返回0
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_memset
; 功能: 使用给定数值初始化内存区域
; 输入参数: r0 = 内存地址
;           r1 = 用于初始化的值
;           r2 = 需要初始化区域的大小(以字(4个字节)为单位表示)
; 输出参数: 无
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_memset		PROC
	EXPORT mos_memset
memset_loop
    stmia r0!,{r1}
    subs r2,#1
    bne memset_loop
    ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_start
; 功能: Mini RTOS启动函数，初始化堆栈、任务等，并调用C代码中出现的mos_init()函数(初始化任务专用)
; 输入参数: 无
; 输出参数: 无
; 注意1: 中断中不能调用
; 注意2: 本函数只能运行一次，之后Mini RTOS启动，一去不回头了 ^_^
;------------------------------------------------------------------------------
mos_start		PROC
	EXPORT	mos_start
	cpsid i                     ; 关中断
	
	ldr r0,=FREE_MEMORY_START	; r0 = 需要初始化内存的首地址
    movs r1,#0					; 使用0清除全部内存
    movs r2,#1
    lsls r2,#(ALL_MEMORY_SHIFT_L - 2)	; 每次清除一个字(4个字节)
	ldr r3,=(NON_CLEAR_AREA/4)	; 计算不需要清除的范围，按字(4个字节)计算
	subs r2,r3					; 相减后得到清除的循环次数
    call mos_memset

	ldr r0,=KERNEL_STACK_TOP	; r0 = 内核栈顶地址
    msr MSP,r0               	; 写内核栈顶地址到MSP指针
    msr PSP,r0                  ; 写内核栈顶地址到PSP指针
    movs r1,#2					; CONTROL寄存器低1位是SP指针模式开关，1使用PSP，0使用MSP
    orrs r1,r1					; 低1位置1
    msr CONTROL, r1				; 写回CONTROL寄存器，改变SP指针模式使用PSP线程模式
    isb                         ; 在执行新指令之前，确保所有前面的指令都已完成

	ldr r0,=NVIC_SYSPRI14		; 设置PendSV优先级为3级最低
	ldr r1,=NVIC_PENDSV_PRI		; 顺带设置SysTICK优先级为0级最高
    str r1,[r0]
	
    call init_idle_task			; 调用空闲任务初始化函数
    call init_task_area			; 调用任务结构数组初始化函数

    ldr r0,=SYSTICK_CTRL
    ldr r1,=0x17700				; 96000 = 1ms基于96MHz主频
    str r1,[r0,#SYSTICK_LOAD]	; 设定SysTICK重装值
    movs r1,#0
    str r1,[r0,#SYSTICK_VAL]	; 设定SysTICK当前值
    movs r1,#7
    str r1,[r0]                 ; 开启SysTICK计数

    cpsie i                     ; 开中断

	call mos_init               ; 调用C代码中的初始化任务函数，或者下面的mos_init函数(死循环，只是用于最小系统查看资源占用)
	ret
				ENDP

infinite_loop	PROC
	b infinite_loop             ; 供init_task_area函数使用的初始化PC值，防止系统跑飞
				ENDP

mos_init		PROC			; 弱定义一个初始化任务函数，什么都不做一直阻塞，方便纯RTOS本身测试rom及ram占用
	EXPORT	mos_init			[WEAK]
initloop
	b initloop
				ENDP

;------------------------------------------------------------------------------
; 函数名: init_idle_task
; 功能: 空闲任务初始化
; 输入参数: 无
; 输出参数: 无
; 注意1: 内部函数
;------------------------------------------------------------------------------
init_idle_task	PROC
    ldr r0,=IDLE_TASK_STACK_TOP		; r0 = 空闲任务栈顶
    movs r1,#0xC1
    rev r1,r1
    subs r0,#8
    str r1,[r0,#4]					; 写入默认的XPSR值(0xC1000000)
    ldr r1,=idle_task+1				; 读空闲任务的代码地址
    str r1,[r0]						; 写入PC值
    subs r0,#(64-8)					; 模拟计算中断返回时的SP值(保存r4~r11开销32字节，自动保存开销32字节)，前面减过8现在少减8
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    strh r0,[r1,#OFS_IDLE_STACK_POS]; 写入内核数组中的空闲任务栈指针
    ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: init_task_area
; 功能: 任务结构数组初始化，用默认值初始化各个任务结构数组，设置默认任务状态、栈指针、PC值
; 输入参数: 无
; 输出参数: 无
; 注意1: 内部函数
;------------------------------------------------------------------------------
init_task_area	PROC
    ldr r0,=KERNEL_MEMORY_START		; r0 = 内核堆空间首地址
	adds r0,#OFS_TASK_ARRAY
    movs r1,#MAX_TASKS-1			; r1 = 最大任务号
    ldr r4,=(TASK_MAX_MEMORY_TOP - 0x20)	; 默认栈指针
init_loop
    lsls r2,r1,#TASK_ENTRY_SHIFT_L
    adds r2,r0						; r2 = 任务结构数组地址

    movs r3,#STATE_INACTIVE
    strb r3,[r2,#TASK_ENTRY_STATE]	; 写入默认任务状态为不活跃(0x00)
    ldr r3, =infinite_loop+1
    str r3,[r2,#TASK_ENTRY_PC]		; 写入默认PC值
    str r4,[r2,#TASK_ENTRY_STACK]	; 写入默认栈指针

    subs r1,#1
    bpl init_loop
	ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_create_task
; 功能: 创建任务
; 输入参数: r0 = 待创建任务的地址
;           r1 = 任务的栈空间顶部地址
;           r2 = 静态局部变量空间大小
;           r3 = 任务的自定义优先级(预留功能，还未实现)
; 输出参数: r0 = 0~7任务号
;           r0 = -1出错
; 注意1: 一个任务的栈空间大小 = 任务的栈空间顶部地址 - 静态局部变量空间大小 - 下一个任务的栈空间顶部地址，创建任务时一定计算准确并留够富裕
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_create_task		PROC
	EXPORT  mos_create_task
    push {r4-r7,lr}
    cpsid i							; 进入临界区，关中断

    ldr r4,=KERNEL_MEMORY_START		; r4 = 内核堆空间首地址
    movs r5,#MAX_TASKS-1			; r5 = 最大任务号

create_loop
    lsls r6,r5, #TASK_ENTRY_SHIFT_L ; r6 = 根据任务号计算任务结构数组的地址
    adds r6,#OFS_TASK_ARRAY         ; r6 = 加上内核数组使用的空间大小
    adds r6,r4						; 加上内核堆首地址后得到R6 = 指向当前的任务结构数组地址

    ldrb r7,[r6,#TASK_ENTRY_STATE]	; r7 = 任务结构数组中的任务状态
    cmp r7,#STATE_INACTIVE			; 判断任务状态是否是不活跃的(0x00)
    bne create_next                 ; 结果不相等表示任务已创建过，跳转到下一个任务结构数组继续查找

	; 比较结果相等，表示这个任务空间可用，下面开始创建任务
	adds r0,#1                      ; 待创建任务的地址加一处理(适用arm thumb模式)，也就是跳过任务函数中第一句汇编代码(一般都是SP指针相关的操作)，直接从第二句开始执行
    str r0,[r6,#TASK_ENTRY_PC]      ; 将任务地址写入任务结构数组中的任务初始PC指针

	subs r1,r2						; r1 = 栈顶地址 - 静态局部变量空间大小，使得栈顶向下移动，预留出供静态局部变量使用的空间
	subs r1,#0x20					; r1 = 再减去PendSV异常中系统自动保存的栈帧大小
	str r1,[r6,#TASK_ENTRY_STACK]	; 将栈顶地址写回任务结构数组中的任务栈指针
	str r0,[r1,#FRAME_PC]			; 将PC值写入到任务栈空间中
    movs r0,#0xC1					; r0 = XPSR的默认值
    rev r0,r0                       ; 0xC1000000 (le <-> be)
;	ldr r0,=0xC1000000				; 小技巧，虽然与上面两句等效，但编译后会多占用4个字节的rom
	str r0,[r1,#FRAME_XPSR]			; 将XPSR的默认值写入到任务栈空间中

    movs r0,#STATE_UNSCHEDULED		; r0 = 没有调度的任务状态(0x01)
    strb r0,[r6,#TASK_ENTRY_STATE]	; 写回任务结构数组中的任务状态

    ldrb r0, [r4,#OFS_NUM_TASKS]	; 读取内核数组中的当前总任务数量
    adds r0,#1						; 总任务数量加一
    strb r0, [r4,#OFS_NUM_TASKS]	; 写回内核数组中的当前总任务数量

    movs r0,r5                      ; 任务创建成功，r0 = 返回0~7任务号
    b create_exit					; 跳转到结束处理

create_next
    subs r5,#1						; r2 = 待创建任务号减一
    bpl create_loop					; 如果待创建任务号大于等于0，跳转继续待创建任务号的创建操作

    movs r0,#0
    mvns r0,r0						; 所有任务都已使用，无法完成创建，出错返回-1
;	ldr r0,=0xFFFFFFFF				; 小技巧，虽然与上面两句等效，但编译后会多占用4个字节的rom

create_exit							; 结束处理
    cpsie i                         ; 退出临界区，开中断
    pop {r4-r7,pc}
					ENDP
					 
;------------------------------------------------------------------------------
; 函数名: mos_ipc_mail_send
; 功能: 向指定任务号线程的邮箱发送邮件，实现线程间通信
; 输入参数: r0 = 任务号
;           r1 = 邮件内容(4个字节)
; 输出参数: r0 = 0xFFFFFFFF出错
; 注意1: 发送的邮件内容为0时是无效的，默认约定0是无邮件状态
; 注意2: 邮件内容可以自己约定好协议，取值范围0x00000001~0xFFFFFFFE
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_ipc_mail_send	PROC
	EXPORT	mos_ipc_mail_send
    ldr r2,=KERNEL_MEMORY_START		; r2 = 内核堆空间首地址
    lsls r0,#TASK_ENTRY_SHIFT_L
    adds r0,#OFS_TASK_ARRAY
    adds r0,r2
    ldr r2,[r0,#TASK_ENTRY_IPC_MAIL]; 读取指定任务号线程的邮件内容
	movs r3,#0
	mvns r3,r3
	cmp r2,r3						; 比较r2是否等于0xFFFFFFFF		
	beq not_send					; 如果相等就不写邮件了，直接报错r0 = 0xFFFFFFFF
    str r1,[r0,#TASK_ENTRY_IPC_MAIL]; 保存到指定任务号线程的邮箱里
	mvns r3,r3
not_send
	mov r0,r3
	
    ret
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_ipc_mail_read
; 功能: 读取本任务线程邮箱内的邮件，实现线程间通信
; 输入参数: 无
; 输出参数: r0 = 邮件内容
;           r0 = 0无邮件
;------------------------------------------------------------------------------
mos_ipc_mail_read	PROC
	EXPORT	mos_ipc_mail_read
    push {lr}						; 这里先保存一下lr的值，在调用完其他函数后再恢复
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    call mos_get_current_task_id
    lsls r0,#TASK_ENTRY_SHIFT_L
    adds r0,#OFS_TASK_ARRAY
    adds r0,r1
    ldr r1,[r0,#TASK_ENTRY_IPC_MAIL]; 读取邮件
	mov r0,r1
    pop {pc}
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_ipc_mail_set
; 功能: 设置本任务线程邮箱内的邮件
; 输入参数: r0 = 邮件内容
; 输出参数: 无
; 注意1: 当设置数据为0时相当于清除邮件
; 注意2: 当设置数据为0xFFFFFFFF时相当于拒收邮件，其他任务就不能再给本任务发送邮件了
;------------------------------------------------------------------------------
mos_ipc_mail_set	PROC
	EXPORT	mos_ipc_mail_set
    push {lr}						; 这里先保存一下lr的值，在调用完其他函数后再恢复
	movs r3,r0
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    call mos_get_current_task_id
    lsls r0,#TASK_ENTRY_SHIFT_L
    adds r0,#OFS_TASK_ARRAY
    adds r0,r1
	movs r2,r3
    str r2,[r0,#TASK_ENTRY_IPC_MAIL]; 设置邮件
    pop {pc}
					ENDP
						
;------------------------------------------------------------------------------
; 函数名: mos_sleep_task
; 功能: 休眠指定任务号
; 输入参数: r0 = 需要休眠的任务号
;           r1 = 需要休眠的毫秒数
; 注意1: 如果指定的任务已经处于休眠状态，则用输入的休眠值覆写
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_sleep_task		PROC
	EXPORT mos_sleep_task
    cpsid i                         ; 关中断
    
    ldr r2,=KERNEL_MEMORY_START		; r2 = 内核堆空间首地址
    lsls r0,#TASK_ENTRY_SHIFT_L
    adds r2,#OFS_TASK_ARRAY
    adds r2,r0
    strh r1,[r2,#TASK_ENTRY_TARGET]	; 覆写任务休眠剩余次数
    ldrb r1,[r2,#TASK_ENTRY_STATE]	; r1 = 任务状态
    cmp r1,#STATE_SLEEPING
    beq sleeping

    movs r1,#STATE_SENT_TO_SLEEP
    strb r1,[r2,#TASK_ENTRY_STATE]	; 改写任务状态为由其他任务休眠

sleeping
    cpsie i                         ; 开中断
    ret
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_sleep
; 功能: 任务休眠
; 输入参数: r0 = 需要休眠的毫秒数
; 输出参数: 无
; 注意1: 中断中不能调用
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_sleep			PROC
	EXPORT mos_sleep
    ldr r2,=KERNEL_MEMORY_START		; r2 = 内核堆空间首地址
    ldrb r1,[r2,#OFS_CURRENT_TASK]	; 获取当前任务号

    lsls r1,#TASK_ENTRY_SHIFT_L
    adds r2,#OFS_TASK_ARRAY
    adds r2,r1
    strh r0,[r2,#TASK_ENTRY_TARGET]	; 写入任务休眠剩余次数
    movs r0,#STATE_SLEEPING
    strb r0,[r2,#TASK_ENTRY_STATE]	; 写入任务状态为休眠态

	movs r0,#0xF3
	mov r12,r0						; 通过休眠程序调用的调度程序，设置r12 = 0xF3作为判断依据
	trigger_pendsv					; 手动触发PendSV异常，用来立即调度任务，不阻塞这样实时性更强一点
    b .                             ; 当休眠时间结束，调度程序会将本任务的PC值依据本指令进行修正跳过，得使任务可以继续运行下去
    ret
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_trigger_pendsv
; 功能: 手动触发PendSV异常，用来立即调度任务
; 输入参数: r0 = 需要设置的判断标示，一般设置为0xF8
; 输出参数: 无
; 注意1: 判断标示一定不能为0xFF，因为0xFF已经有定义为SysTICK调用
;------------------------------------------------------------------------------
mos_trigger_pendsv		PROC
	EXPORT mos_trigger_pendsv
	mov r12,r0						; 通过非内部调用的调度程序，设置r12 = r0(输入参数)作为判断依据
	trigger_pendsv					; 手动触发PendSV异常，用来立即调度任务
	ret
						ENDP
						
;------------------------------------------------------------------------------
; 函数名: mos_get_task_stack_sp
; 功能: 获取当前使用的SP栈指针
; 输入参数: 无
; 输出参数: r0 = SP栈指针 | (r1 << 30)，最高位为0表示是主栈指针(MSP)，为1表示是进程栈指针(PSP)
; 注意1: 如果程序异常时正好调用本函数可能返回的SP栈指针数值是错误的
;------------------------------------------------------------------------------
mos_get_task_stack_sp	PROC
	EXPORT mos_get_task_stack_sp
	movs r0,#2
	mrs r1, CONTROL
	ands r1,r0
	beq control_used_MSP			; CONTROL[1] == 0是主栈指针(MSP)
	mrs r0,psp
	b next
control_used_MSP
	mrs r0,msp
next
	movs r2,#30
	lsls r1,r2						; r1 = r1 << 30
	orrs r0,r1						; r0 = SP栈指针 | (r1 << 30)
	ret
						ENDP
					
;------------------------------------------------------------------------------
; 函数名: mos_get_task_id
; 功能: 获取指定任务地址的任务号
; 输入参数: r0 = 任务地址
; 输出参数: r0 = 任务号
;           r0 = -1出错
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_get_task_id			PROC
	EXPORT mos_get_task_id
    adds r0,#1						; r0 = 任务地址 + 1
    movs r1,#MAX_TASKS-1			; r1 = 最大任务号

get_task_loop
    ldr r3,=KERNEL_MEMORY_START		; r3 = 内核堆空间首地址
    lsls r2,r1,#TASK_ENTRY_SHIFT_L
    adds r2,#OFS_TASK_ARRAY
    adds r2,r3
    
    ldr r3,[r2,#TASK_ENTRY_PC]		; 读取任务初始PC指针
    cmp r0,r3                       ; 判断是否相等
    beq found_it                    ; 结果等于的话已经找到

    subs r1,#1
    bpl get_task_loop               ; 循环查找下一个任务地址

found_it
    movs r0,r1                      ; 返回任务号，没找到时，任务号为-1
    ret
							ENDP

;------------------------------------------------------------------------------
; 函数名: mos_get_current_task_id
; 功能: 获取当前任务号
; 输入参数: 无
; 输出参数: r0 = 当前任务号
;------------------------------------------------------------------------------
mos_get_current_task_id		PROC
	EXPORT mos_get_current_task_id
    ldr r0,=KERNEL_MEMORY_START		; r0 = 内核堆空间首地址
    ldrb r0,[r0,#OFS_CURRENT_TASK]
    ret
							ENDP

;------------------------------------------------------------------------------
; 函数名: mos_get_number_of_tasks
; 功能: 获取创建的任务总量
; 输入参数: 无
; 输出参数: r0 = 任务总量
;------------------------------------------------------------------------------
mos_get_number_of_tasks		PROC
	EXPORT mos_get_number_of_tasks
    ldr r0,=KERNEL_MEMORY_START		; r0 = 内核堆空间首地址
    ldrb r0,[r0,#OFS_NUM_TASKS]
    ret
                 ENDP

;------------------------------------------------------------------------------
; 函数名: mos_get_system_timer
; 功能: 获取SysTICK计数器时间存储值
; 输入参数: 无
; 输出参数: r0 = SysTICK计数器时间存储值
;------------------------------------------------------------------------------
mos_get_system_timer		PROC
	EXPORT mos_get_system_timer
    ldr r0,=KERNEL_MEMORY_START		; r0 = 内核堆空间首地址
    ldr r0,[r0,#OFS_SYS_TIMER]
    ret
                 ENDP

;------------------------------------------------------------------------------
; 函数名: mos_set_system_timer
; 功能: 更新SysTICK计数器时间存储值
; 输入参数: r0 = SysTICK计数器时间存储值新值
; 输出参数: r0 = SysTICK计数器时间存储值旧值
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_set_system_timer		PROC
	EXPORT mos_set_system_timer
    ldr r1,=KERNEL_MEMORY_START			; r1 = 内核堆空间首地址
    ldr r2,[r1,#OFS_SYS_TIMER]
    str r0,[r1,#OFS_SYS_TIMER]
    movs r0,r2
    ret
                 ENDP
					 
;------------------------------------------------------------------------------
; 函数名: mos_get_idle_count
; 功能: 获取周期性CPU占用率
; 输入参数: 无
; 输出参数: r0 = SysTICK计数器时间存储值旧值
; 注意1: 周期固定为1024毫秒，以周期内空闲任务的调度次数为依据
;------------------------------------------------------------------------------
mos_get_idle_count		PROC
	EXPORT mos_get_idle_count
    ldr r0,=KERNEL_MEMORY_START			; r0 = 内核堆空间首地址
    ldrh r0,[r0,#OFS_IDLE_MAXIMUM_COUNT]
    ret
                 ENDP

;------------------------------------------------------------------------------
; 函数名: mos_task_kill
; 功能: 结束指定任务号的任务
; 输入参数: r0 = 指定任务号(0~7)
; 输出参数: 无
; 警告：没有对输入参数进行过多验证，尽量保证参数在正确范围内
;------------------------------------------------------------------------------
mos_task_kill	PROC
	EXPORT mos_task_kill
	cmp r0,#0
	bmi skip_error					; 任务号为负数跳转skip_error直接退出
	cmp r0,#MAX_MUTEX
	bge skip_error					; 任务号 >= MAX_MUTEX 跳转skip_error直接退出
	
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    lsls r2,r0,#TASK_ENTRY_SHIFT_L
    adds r2,#OFS_TASK_ARRAY
    adds r2,r1

    cpsid i							; 关中断
    movs r3,#STATE_INACTIVE
    strb r3,[r2,#TASK_ENTRY_STATE]	; 写入默认任务状态为不活跃(0x00)

    ldr r4,=TASK_MAX_MEMORY_TOP
    subs r4, #0x20
    str r4, [r2,#TASK_ENTRY_STACK]	; 写入默认栈指针

    ldr r3, =infinite_loop+1        ; 获取默认的PC值
    str r3, [r2,#TASK_ENTRY_PC]		; 写入默认PC值

    ldrb r3,[r1,#OFS_NUM_TASKS]		; 读取任务总量
    subs r3,#1                      ; 任务总量减一
    bpl keep_going                  ; 如果任务总量不是负数，跳转保存任务总量
    b .                             ; 能运行到这里，那是绝对凉凉了，Game Over！

keep_going
    strb r3,[r1,#OFS_NUM_TASKS]		; 保存任务总量
    cpsie i							; 开中断
	
skip_error
	ret
				ENDP

;------------------------------------------------------------------------------
; 函数名: mos_task_exit
; 功能: 结束当前任务
; 输入参数: 无
; 输出参数: 无
;------------------------------------------------------------------------------
mos_task_exit	PROC
	EXPORT mos_task_exit
    call mos_get_current_task_id
    call mos_task_kill

	movs r0,#0xF0
	mov r12,r0						; 通过结束任务程序调用的调度程序，设置r12 = 0xF0作为判断依据
	trigger_pendsv					; 手动触发PendSV异常，用来立即调度任务
	b .								; 无限循环，等待PendSV异常调度其他的任务
				ENDP

				ALIGN				; 代码对齐
				LTORG				; 声明一个数据缓冲池(文字池)供Keil编译器使用

;------------------------------------------------------------------------------
; 函数名: mos_mutex_try_lock
; 功能: 尝试锁定互斥锁
; 输入参数: r0 = 互斥锁号0~7，其中7号互斥锁已经分配给mos_malloc函数专用
; 输出参数: r0 = 0锁定成功
;           r0 = 1锁定失败(已被锁定)，不阻塞直接返回
; 注意1: 在空闲任务中只能调用本函数，而不能调用mos_mutex_lock函数
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_mutex_try_lock	PROC
	EXPORT mos_mutex_try_lock
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    movs r3,#1
	lsls r0,#2						; 互斥锁号 = 互斥锁号 * 4，因为互斥锁记录中每4位记录一把锁
    lsls r3,r0						; r3 = 输入的互斥锁号(十进制数表示)转化为二进制位表示，如0x80000000代表7号
	lsls r3,#3						; r7 = 再左移三位，跳过互斥锁记录中锁持有人(任务号)信息位
    
    cpsid i							; 进入临界区，关中断   
	
    ldr r2,[r1,#OFS_MUTEX_STORAGE]	; r2 = 内核数组定义中的互斥锁记录(二进制位表示互斥锁号)
    ands r2,r3						; r2 = 互斥锁记录((二进制位) AND 输入的互斥锁号((二进制位)
    beq mutex_is_free				; 结果为0跳转，说明这个互斥锁号还没使用

    cpsie i							; 退出临界区，开中断，进入互斥锁已使用的处理
	
    movs r0,#1						; 已被锁定返回1
    ret
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_mutex_lock
; 功能: 锁定互斥锁
; 输入参数: r0 = 互斥锁号0~7，其中7号互斥锁已经分配给mos_malloc函数专用
; 输出参数: r0 = 0锁定成功，锁定失败(已被锁定)则一直阻塞并设置任务状态为等待互斥锁，等待调度
; 注意1: 中断中及空闲任务中不能调用，只能由普通任务调用
; 注意2: 本函数有阻塞性质，所以调用时可以不用判断是否锁定成功
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_mutex_lock		PROC
	EXPORT mos_mutex_lock
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    movs r3,#1
	lsls r0,#2						; 互斥锁号 = 互斥锁号 * 4，因为互斥锁记录中每4位记录一把锁
    lsls r3,r0						; r3 = 输入的互斥锁号(十进制数表示)转化为二进制位表示，如0x80000000代表7号
	lsls r3,#3						; r3 = 再左移三位，跳过互斥锁记录中锁持有人(任务号)信息位
    
    cpsid i							; 进入临界区，关中断
    
    ldr r2,[r1,#OFS_MUTEX_STORAGE]	; r2 = 内核数组定义中的互斥锁记录(二进制位表示互斥锁号)
    ands r2,r3						; r2 = 互斥锁记录((二进制位) AND 输入的互斥锁号((二进制位)
    beq mutex_is_free				; 结果为0跳转，说明这个互斥锁号还没使用

    cpsie i							; 退出临界区，开中断，进入互斥锁已使用的处理

    ldrb r2,[r1,#OFS_CURRENT_TASK]  ; 获取当前任务号
    lsls r2,#TASK_ENTRY_SHIFT_L		; 根据任务号计算任务结构数组的地址
    adds r2,#OFS_TASK_ARRAY			; 加上内核数组使用的空间大小
    adds r2,r1                      ; 加上内核堆首地址后得到R2 = 指向当前的任务结构数组地址

	lsrs r0,#2						; 互斥锁号 = 互斥锁号 / 4，恢复十进制数的锁号
    strb r0,[r2,#TASK_ENTRY_MUTEX]	; 将输入的互斥锁号写入任务结构数组中的互斥锁号
    movs r0,#STATE_WAIT_FOR_MUTEX	; 
    strb r0,[r2,#TASK_ENTRY_STATE]	; 将任务状态设为等待互斥锁(0x05)
    
    b .								; 一直阻塞，直到调度任务中跳过
    ret
					ENDP
mutex_is_free		PROC			; 互斥锁加锁处理，mos_mutex_try_lock和mos_mutex_lock函数的公用部分
;    ldr r2,[r1,#OFS_MUTEX_STORAGE]	; r2 = 内核数组定义中的互斥锁记录(二进制位表示互斥锁号)
	movs r2,#0xF					; 准备清除数据，以便清除输入对应锁号的二进制位内容
	lsls r2,r0
	mvns r2,r2

	ldrb r3,[r1,#OFS_CURRENT_TASK]  ; 获取当前任务号
	adds r3,#8						; 在任务号的高一位二进制位上置1
	lsls r3,r0						; 准备输入对应锁号(任务号+加锁)数据
	
	ldr r0,[r1,#OFS_MUTEX_STORAGE]	; 读取内核数组中的互斥锁记录
	ands r2,r0						; 清除输入对应锁号的二进制位内容
	
    orrs r2,r3						; 得到新的输入对应锁号数据
    str r2,[r1,#OFS_MUTEX_STORAGE]	; 写回内核数组中的互斥锁记录

    cpsie i							; 退出临界区，开中断
    movs r0,#0						; 锁定成功返回0
    ret
					ENDP

;------------------------------------------------------------------------------
; 函数名: mos_mutex_unlock
; 功能: 解锁互斥锁
; 输入参数: r0 = 互斥锁号0~7，其中7号互斥锁已经分配给mos_malloc函数专用
; 输出参数: r0 = 0解锁成功
;           r0 = -1解锁失败
; 注意1: 使用时尽量保证加锁+解锁由一个任务完成
; 警告：没有对输入参数进行验证，需要保证参数在正确范围内
;------------------------------------------------------------------------------
mos_mutex_unlock	PROC
	EXPORT mos_mutex_unlock
	push {r4}
    ldr r1,=KERNEL_MEMORY_START		; r1 = 内核堆空间首地址
    movs r3,#0xF
	lsls r0,#2						; 互斥锁号 = 互斥锁号 * 4，因为互斥锁记录中每4个二进制位记录一把锁
    lsls r3,r0
    mvns r3,r3						; r3 = r3位取反
    
    cpsid i

	ldrb r2,[r1,#OFS_CURRENT_TASK]  ; 获取当前任务号
	cmp r2,#0x80					; 判断是否是空闲任务调用的
	beq mutex_unlock_end_proc		; 是空闲任务调用则跳转mutex_unlock_end_proc执行
									; 因为空闲任务调用的情况下一定是空闲任务加锁的，不用再进行持锁人判定，加快处理速度
	
    ldr r4,[r1,#OFS_MUTEX_STORAGE]	; 读取内核数组中的互斥锁记录
	lsrs r4,r0
	movs r0,#7
	ands r4,r0						; r4 = 互斥锁记录中的持锁人信息
	movs r0,#0						; 返回值
	cmp r2,r4						; 判断是否是持锁人本人
	bne	skip_mutex_unlock			; 不是本人不能解锁，返回出错信息(0 - 1 = -1)，跳转skip_mutex_unlock处理

mutex_unlock_end_proc
	; 是持锁人本人，可以解锁
	ldr r2,[r1,#OFS_MUTEX_STORAGE]	; 读取内核数组中的互斥锁记录
    ands r2,r3						; r2 = 在输入互斥锁号的对应4个二进制位上清0
    str r2,[r1,#OFS_MUTEX_STORAGE]	; 写回内核数组中的互斥锁记录
	movs r0,#1						; 返回1 - 1 = 0解锁成功

skip_mutex_unlock
	subs r0,#1
    cpsie i
	pop {r4}
    ret
					ENDP

				ALIGN

				END

;--------------------------------程序结束--------------------------------------
